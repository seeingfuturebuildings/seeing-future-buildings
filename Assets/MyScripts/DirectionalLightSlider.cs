//
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;



//[AddComponentMenu("Scripts/MRTK/Examples/SliderChangeColor")]
public class DirectionalLightSlider : MonoBehaviour
{
    [SerializeField]
    private Transform transformDirectionalLight = null;


    public void OnSliderUpdatedX(SliderEventData eventData)
    {
        transformDirectionalLight.eulerAngles = new Vector3(
            eventData.NewValue * 360.0f,
            transformDirectionalLight.eulerAngles.y,
            transformDirectionalLight.eulerAngles.z);
    }

    public void OnSliderUpdatedY(SliderEventData eventData)
    {
        transformDirectionalLight.eulerAngles = new Vector3(
            transformDirectionalLight.eulerAngles.x,
            eventData.NewValue * 360.0f,
            transformDirectionalLight.eulerAngles.z);
    }

    public void OnSliderUpdatedZ(SliderEventData eventData)
    {
        transformDirectionalLight.eulerAngles = new Vector3(
           transformDirectionalLight.eulerAngles.x,
           transformDirectionalLight.eulerAngles.y,
           eventData.NewValue * 360.0f);
    }
}
