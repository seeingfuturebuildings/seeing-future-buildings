using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Color color;
    
    private float colorHue, colorSaturation, colorValue;  // for debugging which qrcode is controlling the model

    private float t = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        Color.RGBToHSV(color, out colorHue, out colorSaturation, out colorValue);
    }

    // Update is called once per frame
    void Update()
    {
        var material = transform.GetChild(0).gameObject.GetComponent<Renderer>().material;
        material.color = Color.HSVToRGB(colorHue, colorSaturation * (0.5f * Mathf.Cos(0.01f * t++) + 0.5f), colorValue);
    }
}
