using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightAngelInteraction : MonoBehaviour
{   
    public GameObject directionalLight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // float x = Mathf.Clamp(transform.eulerAngles.x, -90.0F, 90.0F);
        Vector3 ea = new Vector3(
            transform.eulerAngles.x,
            transform.eulerAngles.y,
            transform.eulerAngles.z);
        transform.eulerAngles = ea;
        directionalLight.transform.eulerAngles = ea;
    }

    public void showAt(Transform t)
    {
        transform.position = t.position;
    }
}
