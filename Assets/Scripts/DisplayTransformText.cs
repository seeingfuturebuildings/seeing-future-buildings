using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QRTracking;

public class DisplayTransformText : MonoBehaviour
{
    public GameObject qrCodeManager;
    public bool BuildingOrBg = true;
    private GameObject translationText;
    private GameObject rotationText;

    // Start is called before the first frame update
    void Start()
    {
        translationText = transform.Find("TranslationText").gameObject;
        rotationText = transform.Find("RotationText").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var visualizer = qrCodeManager.GetComponent<QRCodesVisualizer>();
        if (BuildingOrBg) {
            translationText.GetComponent<TextMesh>().text = visualizer.GetModelTranslateText();
            rotationText.GetComponent<TextMesh>().text = visualizer.GetModelRotationText();
        }
        else {
            translationText.GetComponent<TextMesh>().text = visualizer.GetBgTranslateText();
            rotationText.GetComponent<TextMesh>().text = visualizer.GetBgRotationText();
        }
    }
}
