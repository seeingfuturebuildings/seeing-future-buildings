﻿using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema;
using Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

using Microsoft.MixedReality.QR;

// #if WINDOWS_UWP
// using Windows.Storage;
// #endif
namespace QRTracking
{
    public class QRCodesVisualizer : MonoBehaviour
    {
        // public GameObject DialogPrefab;
        public string defaultSFBScene; // Relative to StreamingAssets Path

        public GameObject qrCodePrefab;

        // public int numMarkers;

        public float translationDelta = 0.25F;
        public float rotationDelta = 3; // angle

        // public Vector3 ModelOffsetTranslation; // Model's offset from QR Code 0 location
        // public Vector3 ModelOffsetRotation; // Euler angles
        // public Vector3 BgOffsetTranslationToModel;  // Background's offset from model location
        // public Vector3 BgOffsetRotationToModel; 
        
        // public List<Vector3> MarkerOffsetTranslations;  // Each possible QRCode offset from QR0
        // public List<Vector3> MarkerOffsetRotations;  
        
        public GameObject model_and_bg;  // model to be controlled by qr codes detected in the frame
        public GameObject surrounding;
        public GameObject debug;

        public GameObject QRCodeUI;

        private SFBScene sfb_scene;
        private QRCode mainQR = null;
        private System.DateTimeOffset startTime;

        private System.Collections.Generic.SortedDictionary<System.Guid, GameObject> qrCodesObjectsList;
        private bool clearExisting = false;

        struct ActionData
        {
            public enum Type
            {
                Added,
                Updated,
                Removed
            };
            public Type type;
            public Microsoft.MixedReality.QR.QRCode qrCode;

            public ActionData(Type type, Microsoft.MixedReality.QR.QRCode qRCode) : this()
            {
                this.type = type;
                qrCode = qRCode;
            }
        }

        private System.Collections.Generic.Queue<ActionData> pendingActions = new Queue<ActionData>();
        void Awake()
        {

        }

        // Use this for initialization
        void Start()
        {
            Debug.Log("QRCodesVisualizer start");
            startTime = System.DateTimeOffset.Now;
            qrCodesObjectsList = new SortedDictionary<System.Guid, GameObject>();

            QRCodesManager.Instance.QRCodesTrackingStateChanged += Instance_QRCodesTrackingStateChanged;
            QRCodesManager.Instance.QRCodeAdded += Instance_QRCodeAdded;
            QRCodesManager.Instance.QRCodeUpdated += Instance_QRCodeUpdated;
            QRCodesManager.Instance.QRCodeRemoved += Instance_QRCodeRemoved;
            if (qrCodePrefab == null)
            {
                throw new System.Exception("Prefab not assigned");
            }

            if (model_and_bg == null)
            {
                throw new System.Exception("Model not assigned");
            }
            if (QRCodeUI == null) 
            {
                Debug.Log("QRCodeUI not set");
            }

            sfb_scene = new SFBScene();
            LoadScene(Path.Combine(Path.GetFullPath(Application.persistentDataPath), defaultSFBScene));

            Assert.AreEqual(sfb_scene.numMarkers, sfb_scene.MarkerOffsetTranslations.Count);
            Assert.AreEqual(sfb_scene.numMarkers, sfb_scene.MarkerOffsetRotations.Count);
        }

        private void LoadScene(string absolutePath) 
        {
            if (File.Exists(absolutePath))
            {
                var jsonString = File.ReadAllText(absolutePath);
                sfb_scene = SFBScene.FromJson(jsonString);
                
                var baseDirectory = Path.GetDirectoryName(Path.GetDirectoryName(absolutePath));
                var model_path = Path.Combine(baseDirectory, sfb_scene.building_model_path);
                var bg_path = Path.Combine(baseDirectory, sfb_scene.bg_model_path);
                ApplyScene();

                LoadModel(model_path);
                LoadModel(bg_path);
            }
        }
        
        public void SaveSceneTimestamp()
        {
            System.DateTime now = System.DateTime.Now;
            string filename = $"SFBScenes/Scene_{now.ToString("yyMMddHHmmss")}.json";
            var jsonString = sfb_scene.ToJson();
            var wpath = Path.Combine(Path.GetFullPath(Application.persistentDataPath), filename);
            File.WriteAllText(wpath, jsonString);

            // Dialog.Open(DialogPrefab, DialogButtonType.OK, "Scene Saved!", $"Scene config saved to {filename}", false);
        }

//         public async void LoadModelFromFilePicker()
//         {
// #if WINDOWS_UWP
//             var picker = new Windows.Storage.Pickers.FileOpenPicker();
//             picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
//             picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
//             picker.FileTypeFilter.Add(".json");

//             Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
//             if (file != null)
//             {
//                 // Application now has read/write access to the picked file
//                 LoadModel(file.Path);
//             }
// #endif
//         }

//         public async void LoadSceneFromFilePicker()
//         {
// #if WINDOWS_UWP
//             var picker = new Windows.Storage.Pickers.FileOpenPicker();
//             picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
//             picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
//             picker.FileTypeFilter.Add(".json");

//             Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
//             if (file != null)
//             {
//                 // Application now has read/write access to the picked file
//                 LoadScene(file.Path);
//             }
// #endif
//         }

//         public async void SaveSceneFilePicker()
//         {
// #if WINDOWS_UWP
//             var savePicker = new Windows.Storage.Pickers.FileSavePicker();
//             savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
//             // Dropdown of file types the user can save the file as
//             savePicker.FileTypeChoices.Add("SFBScene JSON", new List<string>() { ".json" });
//             // Default file name if the user does not type one in or select a file to replace
//             savePicker.SuggestedFileName = "NewScene.json";

//             Windows.Storage.StorageFile file = await savePicker.PickSaveFileAsync();
//             if (file != null)
//             {
//                 // Prevent updates to the remote version of the file until
//                 // we finish making changes and call CompleteUpdatesAsync.
//                 Windows.Storage.CachedFileManager.DeferUpdates(file);
//                 // write to file
//                 await Windows.Storage.FileIO.WriteTextAsync(file, sfb_scene.ToJson());
//                 // Let Windows know that we're finished changing the file so
//                 // the other app can update the remote version of the file.
//                 // Completing updates may require Windows to ask for user input.
//                 Windows.Storage.Provider.FileUpdateStatus status =
//                     await Windows.Storage.CachedFileManager.CompleteUpdatesAsync(file);
//                 // if (status == Windows.Storage.Provider.FileUpdateStatus.Complete)
//                 // {
//                 //     this.textBlock.Text = "File " + file.Name + " was saved.";
//                 // }
//                 // else
//                 // {
//                 //     this.textBlock.Text = "File " + file.Name + " couldn't be saved.";
//                 // }
//             }
// #endif
//         }

        private async void LoadModel(String absolutePath)
        {
            var model_uri = new Uri($"file://{absolutePath}").AbsoluteUri;
            Response response = new Response();

            try {
                response = await Rest.GetAsync(model_uri, readResponseData: true);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            if (!response.Successful)
            {
                Debug.LogError($"Failed to get glb model from {model_uri}");
                return;
            }
            var gltfModel = GltfUtility.GetGltfObjectFromGlb(response.ResponseData);
            try
            {
                await gltfModel.ConstructAsync();
            }
            catch (Exception e)
            {
                Debug.LogError($"Gltf Construct failed - {e.Message}\n{e.StackTrace}");
                return;
            }
            gltfModel.GameObjectReference.SetActive(false);

            var modelMesh = gltfModel.meshes[0].Mesh;
            var model_meshFilter = model_and_bg.transform.GetChild(0).gameObject.GetComponent<MeshFilter>();
            model_meshFilter.mesh = modelMesh;
        }

        private async void LoadSurrounding(String absolutePath)
        {
            var model_uri = new Uri($"file://{absolutePath}").AbsoluteUri;
            Response response = new Response();

            try {
                response = await Rest.GetAsync(model_uri, readResponseData: true);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            if (!response.Successful)
            {
                Debug.LogError($"Failed to get glb model from {model_uri}");
                return;
            }
            var gltfModel = GltfUtility.GetGltfObjectFromGlb(response.ResponseData);
            try
            {
                await gltfModel.ConstructAsync();
            }
            catch (Exception e)
            {
                Debug.LogError($"Gltf Construct failed - {e.Message}\n{e.StackTrace}");
                return;
            }
            gltfModel.GameObjectReference.SetActive(false);

            var bgMesh = gltfModel.meshes[0].Mesh;
            var bg_meshFilter = surrounding.transform.GetChild(0).gameObject.GetComponent<MeshFilter>();
            var bg_shadow_MeshFilter = surrounding.transform.GetChild(1).gameObject.GetComponent<MeshFilter>();
            
            bg_meshFilter.mesh = bgMesh;
            bg_shadow_MeshFilter.mesh = bgMesh;
        }

        private async void ApplyScene()
        {
            model_and_bg.transform.GetChild(0).localScale = Vector3.one * sfb_scene.building_scale;
            surrounding.transform.localScale = Vector3.one * sfb_scene.bg_scale;
        }

        private void Instance_QRCodesTrackingStateChanged(object sender, bool status)
        {
            if (!status)
            {
                clearExisting = true;
            }
        }

        private void Instance_QRCodeAdded(object sender, QRCodeEventArgs<Microsoft.MixedReality.QR.QRCode> e)
        {
            Debug.Log("QRCodesVisualizer Instance_QRCodeAdded");

            lock (pendingActions)
            {
                pendingActions.Enqueue(new ActionData(ActionData.Type.Added, e.Data));
            }
        }

        private void Instance_QRCodeUpdated(object sender, QRCodeEventArgs<Microsoft.MixedReality.QR.QRCode> e)
        {
            Debug.Log("QRCodesVisualizer Instance_QRCodeUpdated");

            lock (pendingActions)
            {
                pendingActions.Enqueue(new ActionData(ActionData.Type.Updated, e.Data));
            }
        }

        private void Instance_QRCodeRemoved(object sender, QRCodeEventArgs<Microsoft.MixedReality.QR.QRCode> e)
        {
            Debug.Log("QRCodesVisualizer Instance_QRCodeRemoved");

            lock (pendingActions)
            {
                pendingActions.Enqueue(new ActionData(ActionData.Type.Removed, e.Data));
            }
        }

        private void PossiblyRegisterQRCodeObject(ActionData action)
        {   
            // Ignore markers detected before this start
            if (action.qrCode.LastDetectedTime < startTime)
                return;

            bool success = false;
            int qrMarkerIndex = -1;
            try  // QR codes' data should be an int ranging from 0 to numMarker
            {
                qrMarkerIndex = int.Parse(action.qrCode.Data);
                if (0 <= qrMarkerIndex && qrMarkerIndex < sfb_scene.numMarkers) success = true;
            }
            catch (Exception e)
            {
                Debug.Log(String.Format("Not a valid marker qr code: {0}", action.qrCode.Data));
            }
            if (success)
            {
                GameObject qrCodeObject = Instantiate(qrCodePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                qrCodeObject.GetComponent<SpatialGraphNodeTracker>().Id = action.qrCode.SpatialGraphNodeId;
                var qrCode = qrCodeObject.GetComponent<QRCode>();
                qrCode.visualizer = this;
                qrCode.qrCode = action.qrCode;
                qrCode.qrCodeUI = QRCodeUI;
                qrCode.MarkerOffsetTranslation = sfb_scene.MarkerOffsetTranslations[qrMarkerIndex];
                qrCode.MarkerOffsetRotation = sfb_scene.MarkerOffsetRotations[qrMarkerIndex];
                // qrCode.localToQr0 = qrCodeTransforms[0].worldToLocalMatrix * qrCodeTransforms[qrMarkerIndex].localToWorldMatrix;
                // qrCode.localToQr0 = Matrix4x4.identity;
                // Color.RGBToHSV(modelColors[qrMarkerIndex], 
                    // out qrCode.colorHue, out qrCode.colorSaturation, out qrCode.colorValue);
                qrCodesObjectsList.Add(action.qrCode.Id, qrCodeObject);

                if (mainQR == null)
                    mainQR = qrCode;
            }
        }

        private void HandleEvents()
        {
            lock (pendingActions)
            {
                while (pendingActions.Count > 0)
                {
                    var action = pendingActions.Dequeue();
                    if (action.type == ActionData.Type.Added)
                    {
                        PossiblyRegisterQRCodeObject(action);
                    }
                    else if (action.type == ActionData.Type.Updated)
                    {
                        if (!qrCodesObjectsList.ContainsKey(action.qrCode.Id))
                        {
                            PossiblyRegisterQRCodeObject(action);
                        }
                    }
                    else if (action.type == ActionData.Type.Removed)
                    {
                        if (qrCodesObjectsList.ContainsKey(action.qrCode.Id))
                        {
                            Destroy(qrCodesObjectsList[action.qrCode.Id]);
                            qrCodesObjectsList.Remove(action.qrCode.Id);
                        }
                    }
                }
            }
            if (clearExisting)
            {
                clearExisting = false;
                foreach (var obj in qrCodesObjectsList)
                {
                    Destroy(obj.Value);
                }
                qrCodesObjectsList.Clear();

            }
        }
        
        public void updateBuilding(Transform t, Vector3 MarkerOffsetTranslation, Vector3 MarkerOffsetRotation)
        {
            // Apply qr code 0's transform on the model
            if (!model_and_bg.activeSelf) 
                model_and_bg.SetActive(true);
            model_and_bg.transform.FromMatrix(t.localToWorldMatrix);
            model_and_bg.transform.Translate(-MarkerOffsetTranslation);
            model_and_bg.transform.Rotate(-MarkerOffsetRotation);
            model_and_bg.transform.Translate(sfb_scene.ModelOffsetTranslation);
            model_and_bg.transform.Rotate(sfb_scene.ModelOffsetRotation);
            surrounding.transform.localPosition = new Vector3(0, 0, 0);
            surrounding.transform.localRotation = Quaternion.identity;
            surrounding.transform.Translate(sfb_scene.BgOffsetTranslationToModel);
            surrounding.transform.Rotate(sfb_scene.BgOffsetRotationToModel);
        }

        public void ToggleQRTracking()
        {
            foreach (var obj in qrCodesObjectsList)
            {
                obj.Value.GetComponent<QRCode>().ToggleTracking();
            }
        }

        public void ModelTranslate(int direction) // x: 0- 1+ y: 2- 3+ z: 4- 5+
        {   
            if (mainQR != null) {
                float x = translationDelta;
                if (direction % 2 == 0)
                    x *= -1;
                direction /= 2;
                if (direction == 0)
                    sfb_scene.ModelOffsetTranslation += new Vector3(x, 0, 0);
                if (direction == 1)
                    sfb_scene.ModelOffsetTranslation += new Vector3(0, x, 0);
                if (direction == 2)
                    sfb_scene.ModelOffsetTranslation += new Vector3(0, 0, x);
                mainQR.SetBuildingUpdate();
            }
        }
        public void ModelRotate(int direction)
        {
            if (mainQR != null) {
                float x = rotationDelta;
                if (direction % 2 == 0)
                    x *= -1;
                direction /= 2;
                if (direction == 0)
                    sfb_scene.ModelOffsetRotation += new Vector3(x, 0, 0);
                if (direction == 1)
                    sfb_scene.ModelOffsetRotation += new Vector3(0, x, 0);
                if (direction == 2)
                    sfb_scene.ModelOffsetRotation += new Vector3(0, 0, x);
                mainQR.SetBuildingUpdate();
            }
        }

        public void BgTranslate(int direction) // x: 0- 1+ y: 2- 3+ z: 4- 5+
        {   
            if (mainQR != null) {
                float x = translationDelta;
                if (direction % 2 == 0)
                    x *= -1;
                direction /= 2;
                if (direction == 0)
                    sfb_scene.BgOffsetTranslationToModel += new Vector3(x, 0, 0);
                if (direction == 1)
                    sfb_scene.BgOffsetTranslationToModel += new Vector3(0, x, 0);
                if (direction == 2)
                    sfb_scene.BgOffsetTranslationToModel += new Vector3(0, 0, x);
                mainQR.SetBuildingUpdate();
            }
        }
        public void BgRotate(int direction)
        {
            if (mainQR != null) {
                float x = rotationDelta;
                if (direction % 2 == 0)
                    x *= -1;
                direction /= 2;
                if (direction == 0)
                    sfb_scene.BgOffsetRotationToModel += new Vector3(x, 0, 0);
                if (direction == 1)
                    sfb_scene.BgOffsetRotationToModel += new Vector3(0, x, 0);
                if (direction == 2)
                    sfb_scene.BgOffsetRotationToModel += new Vector3(0, 0, x);
                mainQR.SetBuildingUpdate();
            }
        }

        public String GetModelTranslateText()
        {
            return sfb_scene.ModelOffsetTranslation.ToString();
        }

        public String GetModelRotationText()
        {
            return sfb_scene.ModelOffsetRotation.ToString();
        }

        public String GetBgTranslateText()
        {
            return sfb_scene.BgOffsetTranslationToModel.ToString();
        }

        public String GetBgRotationText()
        {
            return sfb_scene.BgOffsetRotationToModel.ToString();
        }

        // Update is called once per frame
        void Update()
        {
            HandleEvents();
        }
    }

}