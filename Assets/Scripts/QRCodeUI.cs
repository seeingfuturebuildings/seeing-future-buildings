using System;
using System.Collections;

using System.Collections.Generic;
using UnityEngine;

namespace QRTracking
{
    public class QRCodeUI : MonoBehaviour
    {
        private QRCode qrcode;

        // Start is called before the first frame update
        void Start()
        {
            hide(); // Hide UI when no QRCode detected
        }

        // Update is called once per frame
        void Update()
        {
            // TODO: hide when move far away
            return;
        }

        public void SetCode(QRCode newcode) 
        {
            qrcode = newcode;
            show();
        }

        public void SetBuildingUpdate()
        {
            if (qrcode != null) 
                qrcode.SetBuildingUpdate();
        }

        public void hide()
        {
            gameObject.SetActive(false);
        }

        public void show()
        {
            gameObject.SetActive(true);
        }
    }
}