using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SFBScene
{
    public string building_model_path = "";
    public string bg_model_path = "";
    public float building_scale = 1;
    public float bg_scale = 1;
    public int numMarkers = 1;

    public Vector3 ModelOffsetTranslation; // Model's offset from QR Code 0 location
    public Vector3 ModelOffsetRotation; // Euler angles
    public Vector3 BgOffsetTranslationToModel;  // Background's offset from model location
    public Vector3 BgOffsetRotationToModel; 
    
    public List<Vector3> MarkerOffsetTranslations;  // Each possible QRCode offset from QR0
    public List<Vector3> MarkerOffsetRotations; 

    public SFBScene()
    {
        ModelOffsetTranslation = Vector3.zero;
        ModelOffsetRotation = new Vector3(90, 0, 0);
        BgOffsetTranslationToModel = Vector3.zero;
        BgOffsetRotationToModel = Vector3.zero;

        MarkerOffsetTranslations = new List<Vector3>{Vector3.zero};
        MarkerOffsetRotations = new List<Vector3>{Vector3.zero};
    }

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    } 

    public static SFBScene FromJson(string jsonString)
    {
        return JsonUtility.FromJson<SFBScene>(jsonString);
    }
}
