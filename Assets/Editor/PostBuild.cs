using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Xml;
using System;

public class PostBuild {
    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load("Build/SeeingFutureBuilding/Package.appxmanifest");
        XmlElement ele = xmlDoc.CreateElement("uap", "Capability", "http://schemas.microsoft.com/appx/manifest/uap/windows10");
        ele.SetAttribute("Name", "documentsLibrary");  
        var node = xmlDoc.DocumentElement.LastChild;
        bool alreadyadded = false;
        if (node.HasChildNodes) 
        {
            for (int i = 0; i < node.ChildNodes.Count; i++) 
            {
                Console.WriteLine(node.ChildNodes[i].Name);
                if (node.ChildNodes[i].Attributes["Name"].Value.Equals("documentsLibrary"))
                    alreadyadded = true;
            }
        }
        if (!alreadyadded)
            node.PrependChild(ele.Clone());  
        xmlDoc.Save("Build/SeeingFutureBuilding/Package.appxmanifest");  
    }
}